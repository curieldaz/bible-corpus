# -*- coding:utf-8 -*-

import numpy as np
import math

def biased_estimator(points, mean):
    n = len(points)
    result = 0
    for point in points:
        result += math.pow(point - mean, 2)
    return result/n

def unbiased_estimator(points, mean):
    n = len(points) - 1
    result = 0
    for point in points:
        result += math.pow(point - mean, 2)
    return result/n

def manual_mean(points):
    n = len(points)
    result = 0
    for point in points:
        result += point
    return result/n

def FMV(freq, f_sim_l, is_unbiased=True):
    lengths = [l for f, l in f_sim_l if f == freq]

    try:
        if len(lengths)==0:
            raise Exception
        #m_l_f = np.mean(lengths)
        m_l_f = manual_mean(lengths)
    except:
        m_l_f = np.nan
    try:
        if is_unbiased:
            if len(lengths)==1:
                raise Exception
            #v_l_f = np.var(lengths, ddof=1)
            v_l_f = unbiased_estimator(lengths, m_l_f)
        else:
            if len(lengths)==0:
                raise Exception
            #v_l_f = np.var(lengths)
            v_l_f = biased_estimator(lengths, m_l_f)
    except:
        v_l_f = np.nan

    return m_l_f, v_l_f


def LMV(length, l_sim_f, is_unbiased=True):
    freqs = [f for l, f in l_sim_f if l == length]

    try:
        if len(freqs)==0:
            raise Exception
        #m_f_l = np.mean(freqs)
        m_f_l = manual_mean(freqs)
    except:
        m_f_l = np.nan
    try:
        if is_unbiased:
            if len(freqs)==1:
                raise Exception
            #v_f_l = np.var(freqs, ddof=1)
            v_f_l = unbiased_estimator(freqs, m_f_l)
        else:
            if len(freqs)==0:
                raise Exception
            #v_f_l = np.var(freqs)
            v_f_l = biased_estimator(freqs, m_f_l)
    except:
        v_f_l = np.nan

    return m_f_l, v_f_l


def FMV_deprecated(f, D, is_unbiased=True):
    '''M(l|f) and V(l|f) for types in D of frequency f'''
    if is_unbiased:
        estimator = statistics.variance
    else:
        estimator = statistics.pvariance

    lengths = [len(x) for x in D.tokens_by_frequency.get(f, [])]

    try:
        m_l_f = statistics.mean(lengths)
    except:
        m_l_f = None
    try:
        v_l_f = estimator(lengths)
    except:
        v_l_f = None
    return m_l_f, v_l_f

def LMV_deprecated(l, D):
    '''M(f|l) and V(f|l) for types in D of length l'''
    m_f_l = 0
    freqs = []
    for word_type in D.tokens_by_length.get(l, []):
        m_f_l += D.tok_frequency[word_type]
        x.append(D.tok_frequency[word_type])
    m_f_l /= len(D.tokens_by_length.get(l, [0]))
    v_f_l = unbiased_estimator(x)
    return m_f_l, v_f_l
