import matplotlib
matplotlib.use("Agg")

from matplotlib import rc
rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)

import matplotlib.pyplot as plt
import numpy as np
import pandas

import os
import sys

cols_to_plot_f = ['rho_f_sim_l',
                'rho_f_sim_m_l_f',
                'rho_f_sim_v_l_f']

legends_f = [r'$f\sim l$',
           r'$f\sim M(l|f)$',
           r'$f\sim V(l|f)$']

cols_to_plot_l = ['rho_l_sim_f',
                'rho_l_sim_m_f_l',
                'rho_l_sim_v_f_l']

legends_l = [r'$l\sim f$',
           r'$l\sim M(f|l)$',
           r'$l\sim V(f|l)$']

all_cols = {"F":cols_to_plot_f,
            "L":cols_to_plot_l}

all_legends = {"F":legends_f,
               "L":legends_l}

order = ["Latin",
         "Greek",
         "Romanian",
         "Italian",
         "Spanish",
         "Portuguese",
         "French",
         "English",
         "Dutch",
         "Afrikaans",
         "German",
         "Swedish",
         "Norwegian",
         "Danish",
         "Icelandic",
         "Polish",
         "Slovak",
         "Croatian",
         "Serbian",
         "Slovene",
         "Czech",
         "Latvian",
         "Lithuanian",
         "Albanian",
         "Romani",
         "Finnish",
         "Estonian",
         "Hungarian",
         "Paite",
         "Vietnamese",
         "Turkish",
         "Basque",
         "Hebrew",
         "Somali",
         "Tachelhit",#"Tashlhiyt",
         "Kabyle",
         "Wolaytta",
         "Ewe",
         "Zulu",
         "Xhosa",
         "Swahili",
         "Wolof",
         "Lukpa",
         "Dinka",
         "Zarma",
         "Malagasy",
         "Uma",
         "Tagalog",
         "Maori",
         "Cebuano",
         "Indonesian",
         "Galela",
         "Cabecar",#"Cabécar",
         "Chinantec",
         "Amuzgo",
         "Nahuatl",
         "Jakalteko",#"Jakaltek",
         "Q'eqchi'",#"K'ekchí",
         "Uspanteco",#"Uspantek",
         "K'iche'",
         "Cakchiquel",
         "Mam",
         "Quichua",#"Quechua",
         "Aguaruna",
         "Achuar",
         "Shuar",
         "Campa",
         "Barasana",
         "Akawaio",
         "Camsa",#"Camsá",
         "Aukan",#"Ndyuka",
         "Creole",
         "Esperanto"]

#x = np.linspace(0.1, 2*np.pi, 10)
#markerline, stemlines, baseline = plt.stem(x, np.cos(x), '-.')
#plt.setp(baseline, 'color', 'r', 'linewidth', 2)
#
#plt.show()

def stem_plot(file_path, 
              output_folder, 
              output_name, 
              col_selector,
              title="",
              ylabel=r"Spearman correlation ($\rho$)"):
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    cols_to_plot = all_cols[col_selector]
    legends = all_legends[col_selector]

    fig = plt.figure(figsize=(11.69, 8.27))
    
    plt.ylabel(ylabel, fontsize=16)
    plt.title(title, fontsize=16)
    ax = plt.gca()
    ax.xaxis.tick_top()
    ax.xaxis.set_tick_params(labeltop='on')
    
    df = pandas.read_csv(file_path,index_col=0)
    #colors = ['m', 'r', 'skyblue']
    markers = ['o', 'gX', 'rs']
    for i in range(len(cols_to_plot)):
        col = cols_to_plot[i]
        points = []
        for j in range(len(order)):
            lang = order[j]
            if not lang in df.index.values:
                continue
            points.append((j, df[col][lang]))
        ndataset = np.array(points)
        ndataset = ndataset[~np.isnan(ndataset).any(axis=1)]
        markerline, stemlines, baseline = plt.stem(
                            np.array(ndataset[:, 0], dtype=np.int32),
                            np.array(ndataset[:, 1], dtype=np.float),
                            #colors[i],
                            markerfmt=markers[i])

    #plt.setp(baseline, color="r", linewidth=2)
    
    plt.xticks(np.array(range(len(order))), order, rotation="vertical", fontsize=7)
    plt.legend(legends, loc="upper right", ncol=len(cols_to_plot))
    plt.savefig(output_folder + output_name)
    fig.clf()
    plt.close()
    return output_folder + output_name

def main():
    assert(len(sys.argv) > 2)
    assert(os.path.exists(sys.argv[2]))

    folder = "../plots/stem_plots/"
    name = sys.argv[1]
    stem_plot(sys.argv[2], folder, name+"_L", "L")
    stem_plot(sys.argv[2], folder, name+"_F", "F")

if __name__ == "__main__":
    main()
