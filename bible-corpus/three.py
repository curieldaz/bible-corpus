# -*- coding:utf-8 -*-

from correlation import correlation_dumps
import numpy as np
import os

from bible import Bible

import sys

order = [#"Latin",
         #"Greek",
         #"Romanian",
         #"Italian",
         #"Spanish",
         #"Portuguese",
         #"French",
         #"English",
         #"Dutch",
         #"Afrikaans",
         #"German",
         #"Swedish",
         #"Norwegian",
         #"Danish",
         #"Icelandic",
         #"Polish",
         #"Slovak",
         #"Croatian",
         #"Serbian",
         #"Slovene",
         #"Czech",
         #"Latvian",
         #"Lithuanian",
         #"Albanian",
         #"Romani",
         #"Finnish",
         #"Estonian",
         #"Hungarian",
         #"Paite",
         #"Vietnamese",
         #"Turkish",
         "Basque",
         #"Hebrew",
         #"Somali",
         #"Tachelhit",#"Tashlhiyt",
         #"Kabyle",
         #"Wolaytta",
         #"Ewe",
         #"Zulu",
         #"Xhosa",
         #"Swahili",
         #"Wolof",
         #"Lukpa",
         #"Dinka",
         #"Zarma",
         #"Malagasy",
         #"Uma",
         #"Tagalog",
         #"Maori",
         #"Cebuano",
         #"Indonesian",
         #"Galela",
         #"Cabecar",#"Cabécar",
         #"Chinantec",
         #"Amuzgo",
         #"Nahuatl",
         #"Jakalteko",#"Jakaltek",
         #"Q'eqchi'",#"K'ekchí",
         #"Uspanteco",#"Uspantek",
         #"K'iche'",
         #"Cakchiquel",
         #"Mam",
         #"Quichua",#"Quechua",
         #"Aguaruna",
         #"Achuar",
         #"Shuar",
         #"Campa",
         #"Barasana",
         #"Akawaio",
         #"Camsa",#"Camsá",
         #"Aukan",#"Ndyuka",
         #"Creole",
         #"Esperanto"
        ]

source_dirs = [#"../bibles/Usable/",
               #"../bibles/Random_SAME_FLEN/",
               "../bibles/Random_GEOM_LEN/"
              ]

res_names = [#"Original",
             #"LFP",
             "NLFP"
             ]

dump_folder = "../dumps/"

def dump_bible_series(languages):
    for i in range(len(source_dirs)):
        # check the language
        for language in languages:
            corpus_dump_folder = dump_folder + res_names[i] + "/"
            # Write dump folder
            language_dump_folder = corpus_dump_folder + language + "/"
            if not os.path.exists(language_dump_folder):
                os.makedirs(language_dump_folder)

            # Read bible
            new_bible = Bible.from_path(source_dirs[i] + language + ".xml")
            if len(new_bible) > 27:
                    new_bible = new_bible.get_new_testament()
            print("({0}) Counted toks: {1}, Reported: {2}, Folder: {3}".format(
                                                new_bible.language,
                                                new_bible.token_count(),
                                                new_bible.reported_word_count,
                                                source_dirs[i]
                                                )
                  )

            # calculating series
            f_types,\
            f_type_lengths,\
            l_types,\
            l_type_frequencies,\
            f_sim_l,\
            f_sim_m_l_f,\
            f_sim_v_l_f_unbiased,\
            l_sim_f,\
            l_sim_m_f_l,\
            l_sim_v_f_l_unbiased = correlation_dumps(new_bible, "unbiased")

            _, _, _, _, _, _,\
            f_sim_v_l_f_biased,\
            _, _,\
            l_sim_v_f_l_biased = correlation_dumps(new_bible, "biased")

            # Write it down
            with open(language_dump_folder + "f_types.csv", "w") as fp:
                for key in f_types.keys():
                    row = "%d;" % key
                    for word_type in f_types[key]:
                        row += "%s, " % word_type
                    row += "\n"
                    fp.write(row)

            with open(language_dump_folder + "f_type_lengths.csv", "w") as fp:
                for key in f_type_lengths.keys():
                    row = "%d;" % key
                    for length in f_type_lengths[key]:
                        row += "%d, " % length
                    row += "\n"
                    fp.write(row)

            with open(language_dump_folder + "l_types.csv", "w") as fp:
                for key in l_types.keys():
                    row = "%d;" % key
                    for word_type in l_types[key]:
                        row += "%s, " % word_type
                    row += "\n"
                    fp.write(row)

            with open(language_dump_folder + "l_type_frequencies.csv", "w") as fp:
                for key in l_type_frequencies.keys():
                    row = "%d;" % key
                    for type_freq in l_type_frequencies[key]:
                        row += "%d, " % type_freq
                    row += "\n"
                    fp.write(row)

            np.savetxt(language_dump_folder + "f_sim_l.csv", f_sim_l, delimiter=", ")
            np.savetxt(language_dump_folder + "f_sim_m_l_f.csv", f_sim_m_l_f, delimiter=", ")
            np.savetxt(language_dump_folder + "f_sim_v_l_f_unbiased.csv", f_sim_v_l_f_unbiased, delimiter=", ")
            np.savetxt(language_dump_folder + "f_sim_v_l_f_biased.csv", f_sim_v_l_f_biased, delimiter=", ")
            np.savetxt(language_dump_folder + "l_sim_f.csv", l_sim_f, delimiter=", ")
            np.savetxt(language_dump_folder + "l_sim_m_f_l.csv", l_sim_m_f_l, delimiter=", ")
            np.savetxt(language_dump_folder + "l_sim_v_f_l_unbiased.csv", l_sim_v_f_l_unbiased, delimiter=", ")
            np.savetxt(language_dump_folder + "l_sim_v_f_l_biased.csv", l_sim_v_f_l_biased, delimiter=", ")

            # Liberate memory
            del new_bible

if __name__ == "__main__":
    dump_bible_series(order)
