import pandas as pd
import os
import sys
import numpy as np


def process_dataframe(csv_file):
    dataframe = pd.read_csv(csv_file)
    #print("MEANS:")
    #print(dataframe.mean())
    #print("\n\nMEDIAN:")
    #print(dataframe.median())
    #print("\n\nMIN:")
    #print(dataframe.min())
    #print("\n\nMAX:")
    #print(dataframe.max())
    
    l_sim_f_lt_l_sim_m_f_l = 0
    l_sim_f_lt_l_sim_v_f_l = 0
    l_sim_m_f_l_lt_l_sim_v_f_l = 0
    l_sim_v_f_l_lt_l_sim_m_f_l = 0
    l_sim_f_lt_l_sim_m_f_l_lt_l_sim_v_f_l = 0
    l_sim_f_lt_l_sim_v_f_l_lt_l_sim_m_f_l = 0
    
    f_sim_l_lt_f_sim_m_l_f = 0
    f_sim_l_lt_f_sim_v_l_f = 0
    f_sim_m_l_f_lt_f_sim_v_l_f = 0
    f_sim_v_l_f_lt_f_sim_m_l_f = 0
    f_sim_v_lt_f_sim_m_l_f_lt_f_sim_v_l_f = 0
    f_sim_v_lt_f_sim_v_l_f_lt_f_sim_m_l_f = 0
    
    for language in dataframe['Unnamed: 0'].tolist():
        row = dataframe[dataframe['Unnamed: 0'] == language]
        rho_f_sim_l = row.iloc[0]['rho_f_sim_l']
        rho_f_sim_m_l_f = row.iloc[0]['rho_f_sim_m_l_f']
        rho_f_sim_v_l_f = row.iloc[0]['rho_f_sim_v_l_f']
        
        rho_l_sim_f = row.iloc[0]['rho_l_sim_f']
        rho_l_sim_m_f_l = row.iloc[0]['rho_l_sim_m_f_l']
        rho_l_sim_v_f_l = row.iloc[0]['rho_l_sim_v_f_l']

        if abs(rho_l_sim_f) < abs(rho_l_sim_m_f_l):
            #print("l_sim_f < l_sim_m_f_l", language)
            l_sim_f_lt_l_sim_m_f_l += 1
        if abs(rho_l_sim_f) < abs(rho_l_sim_v_f_l): 
            #print("l_sim_f < l_sim_v_f_l", language)
            l_sim_f_lt_l_sim_v_f_l += 1
        if abs(rho_l_sim_m_f_l) < abs(rho_l_sim_v_f_l): 
            #print("l_sim_m_f_l < l_sim_v_f_l", language)
            l_sim_m_f_l_lt_l_sim_v_f_l += 1
        else:
            #print("l_sim_v_f_l < l_sim_m_f_l", language)
            l_sim_v_f_l_lt_l_sim_m_f_l += 1
        if abs(rho_l_sim_f) < abs(rho_l_sim_m_f_l) < abs(rho_l_sim_v_f_l):
            l_sim_f_lt_l_sim_m_f_l_lt_l_sim_v_f_l += 1
        if abs(rho_l_sim_f) < abs(rho_l_sim_v_f_l) < abs(rho_l_sim_m_f_l):
            l_sim_f_lt_l_sim_v_f_l_lt_l_sim_m_f_l += 1
        
        if abs(rho_f_sim_l) < abs(rho_f_sim_m_l_f):
            #print("f_sim_l < f_sim_m_l_f", language)
            f_sim_l_lt_f_sim_m_l_f += 1
        if abs(rho_f_sim_l) < abs(rho_f_sim_v_l_f): 
            #print("f_sim_l < f_sim_v_l_f", language)
            f_sim_l_lt_f_sim_v_l_f += 1
        if abs(rho_f_sim_m_l_f) < abs(rho_f_sim_v_l_f): 
            #print("f_sim_m_l_f < f_sim_v_l_f", language)
            f_sim_m_l_f_lt_f_sim_v_l_f += 1
        else:
            #print("f_sim_v_l_f < f_sim_m_l_f", language)
            f_sim_v_l_f_lt_f_sim_m_l_f += 1
        if abs(rho_f_sim_l) < abs(rho_f_sim_m_l_f) < abs(rho_f_sim_v_l_f): 
            f_sim_v_lt_f_sim_m_l_f_lt_f_sim_v_l_f += 1
        if abs(rho_f_sim_l) < abs(rho_f_sim_v_l_f) < abs(rho_f_sim_m_l_f): 
            f_sim_v_lt_f_sim_v_l_f_lt_f_sim_m_l_f += 1

    try:
        estimator, corpus = csv_file.split("/")[-1].split(".")[0].split("_") 
    except:
        import ipdb;ipdb.set_trace()
    print("\nEstimator: %s, Corpus: %s" % (estimator, corpus))
    
    print("\n")
    print("f ~ l < f ~ M(l|f): %d of %d or %f" % (f_sim_l_lt_f_sim_m_l_f, len(dataframe), f_sim_l_lt_f_sim_m_l_f / len(dataframe))) 
    print("f ~ l < f ~ V(l|f): %d of %d or %f" % (f_sim_l_lt_f_sim_v_l_f, len(dataframe), f_sim_l_lt_f_sim_v_l_f / len(dataframe))) 
    print("f ~ M(l|f) < f ~ V(l|f): %d of %d or %f" % (f_sim_m_l_f_lt_f_sim_v_l_f, len(dataframe), f_sim_m_l_f_lt_f_sim_v_l_f / len(dataframe))) 
    print("f ~ V(l|f) < f ~ M(l|f): %d of %d or %f" % (f_sim_v_l_f_lt_f_sim_m_l_f, len(dataframe), f_sim_v_l_f_lt_f_sim_m_l_f / len(dataframe))) 
    print("f ~ l < f ~ M(l|f) < f ~ V(l|f): %d of %d or %f" % (f_sim_v_lt_f_sim_m_l_f_lt_f_sim_v_l_f, len(dataframe), f_sim_v_lt_f_sim_m_l_f_lt_f_sim_v_l_f / len(dataframe)))
    print("f ~ l < f ~ V(l|f) < f ~ M(l|f): %d of %d or %f" % (f_sim_v_lt_f_sim_v_l_f_lt_f_sim_m_l_f, len(dataframe), f_sim_v_lt_f_sim_v_l_f_lt_f_sim_m_l_f / len(dataframe)))

    print("\n")
    print("l ~ f < l ~ M(f|l): %d of %d or %f" % (l_sim_f_lt_l_sim_m_f_l, len(dataframe), l_sim_f_lt_l_sim_m_f_l / len(dataframe))) 
    print("l ~ f < l ~ V(f|l): %d of %d or %f" % (l_sim_f_lt_l_sim_v_f_l, len(dataframe), l_sim_f_lt_l_sim_v_f_l / len(dataframe))) 
    print("l ~ M(f|l) < l ~ V(f|l): %d of %d or %f" % (l_sim_m_f_l_lt_l_sim_v_f_l, len(dataframe), l_sim_m_f_l_lt_l_sim_v_f_l / len(dataframe))) 
    print("l ~ V(f|l) < l ~ M(f|l): %d of %d or %f" % (l_sim_v_f_l_lt_l_sim_m_f_l, len(dataframe), l_sim_v_f_l_lt_l_sim_m_f_l/len(dataframe))) 
    print("l ~ f < l ~ M(f|l) < l ~ V(f|l): %d of %d or %f" % (l_sim_f_lt_l_sim_m_f_l_lt_l_sim_v_f_l, len(dataframe),  l_sim_f_lt_l_sim_m_f_l_lt_l_sim_v_f_l / len(dataframe)))
    print("l ~ f < l ~ V(f|l) < l ~ M(f|l): %d of %d or %f" % (l_sim_f_lt_l_sim_v_f_l_lt_l_sim_m_f_l, len(dataframe), l_sim_f_lt_l_sim_v_f_l_lt_l_sim_m_f_l / len(dataframe)))

    #import ipdb;ipdb.set_trace() 
    described_csv_name = "/".join(csv_file.split("/")[0:-1]) + "/"
    described_csv_name += "described_" + csv_file.split("/")[-1].split(
                                                            ".")[0] + ".csv"
    described_dataframe = dataframe.describe().iloc[[1,2,3,7]]
    median_row = dataframe.median().to_frame().transpose().rename(index={0:'median'})
    described_dataframe = pd.concat([described_dataframe, median_row])
    described_dataframe.to_csv(described_csv_name)
    
    results = {}
    results['l_sim_f_lt_l_sim_m_f_l'] = l_sim_f_lt_l_sim_m_f_l
    results['l_sim_f_lt_l_sim_v_f_l'] = l_sim_f_lt_l_sim_v_f_l
    results['l_sim_m_f_l_lt_l_sim_v_f_l'] = l_sim_m_f_l_lt_l_sim_v_f_l
    results['l_sim_v_f_l_lt_l_sim_m_f_l'] = l_sim_v_f_l_lt_l_sim_m_f_l
    results['l_sim_f_lt_l_sim_m_f_l_lt_l_sim_v_f_l'] = l_sim_f_lt_l_sim_m_f_l_lt_l_sim_v_f_l
    results['l_sim_f_lt_l_sim_v_f_l_lt_l_sim_m_f_l'] = l_sim_f_lt_l_sim_v_f_l_lt_l_sim_m_f_l
    
    results['f_sim_l_lt_f_sim_m_l_f'] = f_sim_l_lt_f_sim_m_l_f
    results['f_sim_l_lt_f_sim_v_l_f'] = f_sim_l_lt_f_sim_v_l_f
    results['f_sim_m_l_f_lt_f_sim_v_l_f'] = f_sim_m_l_f_lt_f_sim_v_l_f
    results['f_sim_v_l_f_lt_f_sim_m_l_f'] = f_sim_v_l_f_lt_f_sim_m_l_f
    results['f_sim_v_lt_f_sim_m_l_f_lt_f_sim_v_l_f'] = f_sim_v_lt_f_sim_m_l_f_lt_f_sim_v_l_f
    results['f_sim_v_lt_f_sim_v_l_f_lt_f_sim_m_l_f'] = f_sim_v_lt_f_sim_v_l_f_lt_f_sim_m_l_f

    return results
