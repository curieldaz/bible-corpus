import gc

import matplotlib
matplotlib.use("Agg")

from matplotlib import rc
#rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
rc('font',**{'family':'serif','serif':['Palatino'], 'size':18})
rc('text', usetex=True)

import matplotlib.pyplot as plt
import pandas as pd
import os
import argparse
import sys
import numpy as np

output_folder = "../plots/spearman_plots/"

filenames = [
        "biased_Original.csv",
        "biased_LFP.csv",
        "biased_NLFP.csv",
        "unbiased_Original.csv",
        "unbiased_LFP.csv",
        "unbiased_NLFP.csv",
        ]

def main(path, save=True):
    if not os.path.exists(path):
        raise OSError("Not a valid file")

    folder = output_folder
    if not os.path.exists(folder):
        os.makedirs(folder + "Unbiased/")
        os.makedirs(folder + "Biased/")


    if os.path.isdir(path):
        original_df_biased = pd.read_csv(path + filenames[0], index_col=0)
        lfp_df_biased = pd.read_csv(path + filenames[1], index_col=0)
        nlfp_df_biased = pd.read_csv(path + filenames[2], index_col=0)
        original_df_unbiased = pd.read_csv(path + filenames[3], index_col=0)
        lfp_df_unbiased = pd.read_csv(path + filenames[4], index_col=0)
        nlfp_df_unbiased = pd.read_csv(path + filenames[5], index_col=0)

        corpus_legends = ["Original", "LFP", "NLFP"]

        labels = {
                'rho_l_sim_f' : r'Spearman $\rho$ for $l \sim f$',
                'rho_f_sim_l' : r'Spearman $\rho$ for $f \sim l$',
                'rho_l_sim_m_f_l' : r'Spearman $\rho$ for $l \sim M(f|l)$',
                'rho_f_sim_m_l_f' : r'Spearman $\rho$ for $f \sim M(l|f)$',
                'rho_l_sim_v_f_l' : r'Spearman $\rho$ for $l \sim V(f|l)$',
                'rho_f_sim_v_l_f' : r'Spearman $\rho$ for $f \sim V(l|f)$',
                }

        # three corpus
        for x, y in [
                ('rho_l_sim_f', 'rho_l_sim_m_f_l'),
                ('rho_l_sim_f', 'rho_l_sim_v_f_l'),
                ('rho_l_sim_m_f_l', 'rho_l_sim_v_f_l'),
                ('rho_f_sim_l', 'rho_f_sim_m_l_f'),
                ('rho_f_sim_l', 'rho_f_sim_v_l_f'),
                ('rho_f_sim_m_l_f', 'rho_f_sim_v_l_f'),
                ]:

            if save:
                fig = plt.figure(figsize=(11.69, 8.27))

            #unbiased
            points = original_df_unbiased[[x, y]].to_numpy()
            plt.plot(points[:, 0],
                     points[:, 1],
                     "o")

            points = lfp_df_unbiased[[x, y]].to_numpy()
            plt.plot(points[:, 0],
                     points[:, 1],
                     "^")

            points = nlfp_df_unbiased[[x, y]].to_numpy()
            plt.plot(points[:, 0],
                     points[:, 1],
                     "x")

            #ax = plt.gca()
            #diag_line, = ax.plot(ax.get_xlim(), ax.get_ylim(), ls="--", c=".3")

            plt.legend(corpus_legends, loc="upper right")
            plt.ylabel(labels[y], fontsize=18)
            plt.xlabel(labels[x], fontsize=18)

            if "V(" in y:
                plt.title(r"Unbiased Estimator ($V_{Unbiased}$)", fontsize=22)
            if save:
                plt.savefig(folder + "Unbiased/" + x + "_" + y)
                fig.clf()
            else:
                plt.show()

            plt.close()

            if save:
                fig = plt.figure(figsize=(11.69, 8.27))

            #biased
            points = original_df_biased[[x, y]].to_numpy()
            plt.plot(points[:, 0],
                     points[:, 1],
                     "o")

            points = lfp_df_biased[[x, y]].to_numpy()
            plt.plot(points[:, 0],
                     points[:, 1],
                     "^")

            points = nlfp_df_biased[[x, y]].to_numpy()
            plt.plot(points[:, 0],
                     points[:, 1],
                     "x")

            #ax = plt.gca()
            #diag_line, = ax.plot(ax.get_xlim(), ax.get_ylim(), ls="--", c=".3")

            plt.legend(corpus_legends, loc="upper right")
            plt.ylabel(labels[y], fontsize=18)
            plt.xlabel(labels[x], fontsize=18)

            if "V(" in y:
                plt.title(r"Biased Estimator ($V_{Biased}$)", fontsize=22)
            if save:
                plt.savefig(folder + "Biased/" + x + "_" + y)
                fig.clf()
            else:
                plt.show()

            plt.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generate spearman rho graphs.')
    parser.add_argument('-f', '--folder', help='input folder')
    args = parser.parse_args()
    main(args.folder)
