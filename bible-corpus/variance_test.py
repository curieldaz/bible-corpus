import pandas as pd
import numpy as np
import scipy.stats as ss
from bible import Bible

import matplotlib
import matplotlib.pyplot as plt
matplotlib.use("macosx")
import os

import random

order = ["Latin",
         "Greek",
         "Romanian",
         "Italian",
         "Spanish",
         "Portuguese",
         "French",
         "English",
         "Dutch",
         "Afrikaans",
         "German",
         "Swedish",
         "Norwegian",
         "Danish",
         "Icelandic",
         "Polish",
         "Slovak",
         "Croatian",
         "Serbian",
         "Slovene",
         "Czech",
         "Latvian",
         "Lithuanian",
         "Albanian",
         "Romani",
         "Finnish",
         "Estonian",
         "Hungarian",
         "Paite",
         "Vietnamese",
         "Turkish",
         "Basque",
         "Hebrew",
         "Somali",
         "Tachelhit",#"Tashlhiyt",
         "Kabyle",
         "Wolaytta",
         "Ewe",
         "Zulu",
         "Xhosa",
         "Swahili",
         "Wolof",
         "Lukpa",
         "Dinka",
         "Zarma",
         "Malagasy",
         "Uma",
         "Tagalog",
         "Maori",
         "Cebuano",
         "Indonesian",
         "Galela",
         "Cabecar",#"Cabécar",
         "Chinantec",
         "Amuzgo",
         "Nahuatl",
         "Jakalteko",#"Jakaltek",
         "Q'eqchi'",#"K'ekchí",
         "Uspanteco",#"Uspantek",
         "K'iche'",
         "Cakchiquel",
         "Mam",
         "Quichua",#"Quechua",
         "Aguaruna",
         "Achuar",
         "Shuar",
         "Campa",
         "Barasana",
         "Akawaio",
         "Camsa",#"Camsá",
         "Aukan",#"Ndyuka",
         "Creole",
         "Esperanto"
        ]

def spearman(points):
    return ss.spearmanr(np.array(points), nan_policy='omit')

if __name__ == '__main__':
    dumps_folder = "../dumps/"

    corpora = ["LFP", "NLFP", "Original"]

    for corpus in corpora:
        corpus_folder = dumps_folder + corpus + "/"
        for language in order:
            language_folder = corpus_folder + language + "/"
            if not os.path.exists(language_folder):
                continue

            # load relevant files
            #f_sim_l = np.loadtxt(language_folder + "f_sim_l.csv",
            #                     delimiter=", ")
            f_sim_m_l_f = np.loadtxt(language_folder + "f_sim_m_l_f.csv",
                                     delimiter=", ")
            f_sim_v_l_f_unbiased = np.loadtxt(language_folder +\
                                              "f_sim_v_l_f_unbiased.csv",
                                              delimiter=", ")
            f_sim_v_l_f_biased = np.loadtxt(language_folder +\
                                            "f_sim_v_l_f_biased.csv",
                                            delimiter=", ")

            #l_sim_f =  np.loadtxt(language_folder + "l_sim_f.csv",
            #                      delimiter=", ")
            l_sim_m_f_l = np.loadtxt(language_folder + "l_sim_m_f_l.csv",
                                     delimiter=", ")
            l_sim_v_f_l_unbiased =  np.loadtxt(language_folder +\
                                               "l_sim_v_f_l_unbiased.csv",
                                               delimiter=", ")
            l_sim_v_f_l_biased =  np.loadtxt(language_folder +\
                                             "l_sim_v_f_l_biased.csv",
                                             delimiter=", ")

            relationship = []
            with open(language_folder + "l_type_frequencies.csv", "r") as fp:
                for line in fp.readlines():
                    line = line.replace(" ","").replace("\n", "" )[:-1]
                    try:
                        length, frequencies = line.split(";")
                    except:
                        continue
                    length = int(length)
                    frequencies = [int(f) for f in frequencies.split(",")]

                    # testing l_sim_v_f_l biased
                    index = np.where(l_sim_v_f_l_biased[:,0]==length)[0][0]
                    value = l_sim_v_f_l_biased[index][1]
                    try:
                        assert(np.var(frequencies) == value)
                    except:
                        print(np.var(frequencies), value)

                    biased_value = value

                    # testing l_sim_v_f_l unbiased
                    index = np.where(l_sim_v_f_l_unbiased[:,0]==length)[0][0]
                    value = l_sim_v_f_l_unbiased[index][1]
                    try:
                        assert(np.var(frequencies, ddof=1) == value)
                    except:
                        try:
                            assert(np.isnan(np.var(frequencies, ddof=1)))
                            assert(np.isnan(value))
                        except:
                            print(np.var(frequencies, ddof=1), value)

                    unbiased_value = value

                    # testing l_sim_m_f_l
                    index = np.where(l_sim_m_f_l[:,0]==length)[0][0]
                    value = l_sim_m_f_l[index][1]
                    assert(np.mean(frequencies) == value)

                    mean_value = value

                    relationship.append((mean_value, biased_value))
            plt.title(corpus)
            plt.plot(relationship)
            #plt.show()

            with open(language_folder + "f_type_lengths.csv", "r") as fp:
                for line in fp.readlines():
                    line = line.replace(" ","").replace("\n", "" )[:-1]
                    try:
                        frequency, lengths = line.split(";")
                    except:
                        continue
                    frequency = int(frequency)
                    lengths = [int(l) for l in lengths.split(",")]

                    # testing f_sim_v_l_f biased
                    index = np.where(f_sim_v_l_f_biased[:,0]==frequency)[0][0]
                    value = f_sim_v_l_f_biased[index][1]
                    try:
                        assert(np.var(lengths) == value)
                    except:
                        print(np.var(lengths), value)

                    # testing f_sim_v_l_f unbiased
                    index = np.where(f_sim_v_l_f_unbiased[:,0]==frequency)[0][0]
                    value = f_sim_v_l_f_unbiased[index][1]
                    try:
                        assert(np.var(lengths, ddof=1) == value)
                    except:
                        try:
                            assert(np.isnan(np.var(lengths, ddof=1)))
                            assert(np.isnan(value))
                        except:
                            print(np.var(lengths, ddof=1), value)

                    # testing f_sim_m_l_f
                    index = np.where(f_sim_m_l_f[:,0]==frequency)[0][0]
                    value = f_sim_m_l_f[index][1]
                    try:
                        assert(np.mean(lengths) == value)
                    except:
                        import ipdb;ipdb.set_trace()

            biased_all = pd.read_csv("../results/biased_%s.csv" % corpus, index_col=0)
            unbiased_all = pd.read_csv("../results/unbiased_%s.csv" % corpus, index_col=0)

            # check against known results
            rho, p_value = spearman(f_sim_v_l_f_biased)
            try:
                assert(biased_all.loc[language]['rho_f_sim_v_l_f'] == rho)
            except:
                print(biased_all.loc[language]['rho_f_sim_v_l_f'], rho)
            try:
                assert(biased_all.loc[language]['p_f_sim_v_l_f'] == p_value)
            except:
                print(biased_all.loc[language]['p_f_sim_v_l_f'], p_value)

            rho, p_value = spearman(f_sim_v_l_f_unbiased)
            try:
                assert(unbiased_all.loc[language]['rho_f_sim_v_l_f'] == rho)
            except:
                print(unbiased_all.loc[language]['rho_f_sim_v_l_f'], rho)
            try:
                assert(unbiased_all.loc[language]['p_f_sim_v_l_f'] == p_value)
            except:
                print(unbiased_all.loc[language]['p_f_sim_v_l_f'], p_value)

            rho, p_value = spearman(f_sim_m_l_f)
            try:
                assert(biased_all.loc[language]['rho_f_sim_m_l_f'] == rho)
            except:
                print(biased_all.loc[language]['rho_f_sim_m_l_f'], rho)
            try:
                assert(biased_all.loc[language]['p_f_sim_m_l_f'] == p_value)
            except:
                print(biased_all.loc[language]['p_f_sim_m_l_f'], p_value)
            try:
                assert(unbiased_all.loc[language]['rho_f_sim_m_l_f'] == rho)
            except:
                print(unbiased_all.loc[language]['rho_f_sim_m_l_f'], rho)
            try:
                assert(unbiased_all.loc[language]['p_f_sim_m_l_f'] == p_value)
            except:
                print(unbiased_all.loc[language]['p_f_sim_m_l_f'], p_value)
            
            rho, p_value = spearman(l_sim_v_f_l_biased)
            try:
                assert(biased_all.loc[language]['rho_l_sim_v_f_l'] == rho)
            except:
                print(biased_all.loc[language]['rho_l_sim_v_f_l'], rho)
            try:
                assert(biased_all.loc[language]['p_l_sim_v_f_l'] == p_value)
            except:
                print(biased_all.loc[language]['p_l_sim_v_f_l'], p_value)

            rho, p_value = spearman(l_sim_v_f_l_unbiased)
            try:
                assert(unbiased_all.loc[language]['rho_l_sim_v_f_l'] == rho)
            except:
                print(unbiased_all.loc[language]['rho_l_sim_v_f_l'], rho)
            try:
                assert(unbiased_all.loc[language]['p_l_sim_v_f_l'] == p_value)
            except:
                print(unbiased_all.loc[language]['p_l_sim_v_f_l'], p_value)

            rho, p_value = spearman(l_sim_m_f_l)
            try:
                assert(biased_all.loc[language]['rho_l_sim_m_f_l'] == rho)
            except:
                print(biased_all.loc[language]['rho_l_sim_m_f_l'], rho)
            try:
                assert(biased_all.loc[language]['p_l_sim_m_f_l'] == p_value)
            except:
                print(biased_all.loc[language]['p_l_sim_m_f_l'], p_value)

            try:
                assert(unbiased_all.loc[language]['rho_l_sim_m_f_l'] == rho)
            except:
                print(unbiased_all.loc[language]['rho_l_sim_m_f_l'], rho)
            try:
                assert(unbiased_all.loc[language]['p_l_sim_m_f_l'] == p_value)
            except:
                print(unbiased_all.loc[language]['p_l_sim_m_f_l'], p_value)
