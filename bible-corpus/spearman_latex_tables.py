import pandas as pd
import os
import sys
import numpy as np
import string

def create_results_latex_table(unbiased_csv_path, biased_csv_path, case_label):
    if not os.path.exists(unbiased_csv_path) or \
       not unbiased_csv_path.endswith(".csv"):
        raise TypeError("Not a correct csv file: %s" % unbiased_csv_path )

    if not os.path.exists(biased_csv_path) or \
       not biased_csv_path.endswith(".csv"):
        raise TypeError("Not a correct csv file: %s" % biased_csv_path )

    unbiased_dataframe = pd.read_csv(unbiased_csv_path, index_col=0)
    biased_dataframe = pd.read_csv(biased_csv_path, index_col=0)

    unbiased_table_contents = ""
    biased_table_contents = ""
    for variable in ['min', 'mean', 'median', 'max', 'std']:
        unbiased_row = unbiased_dataframe.loc[variable]
        biased_row = biased_dataframe.loc[variable]

        unbiased_table_contents += variable
        biased_table_contents += variable
        for col in ['rho_f_sim_l',
                    'rho_f_sim_m_l_f',
                    'rho_f_sim_v_l_f',
                    'rho_l_sim_f',
                    'rho_l_sim_m_f_l',
                    'rho_l_sim_v_f_l']:
            unbiased_table_contents += "& %.2e" % unbiased_row[col]
            biased_table_contents += "& %.2e" % biased_row[col]
        unbiased_table_contents += " \\\\\\hline\n"
        biased_table_contents += " \\\\\\hline\n"

    with open("../res/spearman_table.tex", "r") as f:
        template = f.read()

    template = string.Template(template)

    unbiased_table = template.substitute({'corpus' : case_label,
                                 'spearman_table_contents' : unbiased_table_contents,
                                 'comparison_table_contents' : '' ,
                                 'bias' : 'unbiased'})
    result_path = os.path.dirname(unbiased_csv_path) + "/latex_tables/" + \
                  "unbiased_spearman_" + case_label + ".tex"
    with open(result_path, "w") as f:
        f.write(unbiased_table)
        f.flush()

    biased_table = template.substitute({'corpus' : case_label,
                                 'spearman_table_contents' : biased_table_contents,
                                 'comparison_table_contents' : '',
                                 'bias' : 'biased'})
    result_path = os.path.dirname(biased_csv_path) + "/latex_tables/" + \
                  "biased_spearman_" + case_label + ".tex"
    with open(result_path, "w") as f:
        f.write(biased_table)
        f.flush()

create_results_latex_table("../results/described_unbiased_Original.csv",
                           "../results/described_biased_Original.csv",
                           "Original")
create_results_latex_table("../results/described_unbiased_LFP.csv",
                           "../results/described_biased_LFP.csv",
                           "LFP")
create_results_latex_table("../results/described_unbiased_NLFP.csv",
                           "../results/described_biased_NLFP.csv",
                           "NLFP")
