# -*- coding:utf-8 -*-

from graphs import plot_multiple_points
from correlation import graph, correlation_analysis
import pandas as pd
import os
import pprint
from bible import Bible

from stem_plots import stem_plot

import multiprocessing

import sys

pp = pprint.PrettyPrinter(indent=4)

order = ["Latin",
         "Greek",
         "Romanian",
         "Italian",
         "Spanish",
         "Portuguese",
         "French",
         "English",
         "Dutch",
         "Afrikaans",
         "German",
         "Swedish",
         "Norwegian",
         "Danish",
         "Icelandic",
         "Polish",
         "Slovak",
         "Croatian",
         "Serbian",
         "Slovene",
         "Czech",
         "Latvian",
         "Lithuanian",
         "Albanian",
         "Romani",
         "Finnish",
         "Estonian",
         "Hungarian",
         "Paite",
         "Vietnamese",
         "Turkish",
         "Basque",
         "Hebrew",
         "Somali",
         "Tachelhit",#"Tashlhiyt",
         "Kabyle",
         "Wolaytta",
         "Ewe",
         "Zulu",
         "Xhosa",
         "Swahili",
         "Wolof",
         "Lukpa",
         "Dinka",
         "Zarma",
         "Malagasy",
         "Uma",
         "Tagalog",
         "Maori",
         "Cebuano",
         "Indonesian",
         "Galela",
         "Cabecar",#"Cabécar",
         "Chinantec",
         "Amuzgo",
         "Nahuatl",
         "Jakalteko",#"Jakaltek",
         "Q'eqchi'",#"K'ekchí",
         "Uspanteco",#"Uspantek",
         "K'iche'",
         "Cakchiquel",
         "Mam",
         "Quichua",#"Quechua",
         "Aguaruna",
         "Achuar",
         "Shuar",
         "Campa",
         "Barasana",
         "Akawaio",
         "Camsa",#"Camsá",
         "Aukan",#"Ndyuka",
         "Creole",
         "Esperanto"
        ] 

column_headers = ["rho_f_sim_l", "p_f_sim_l",
                  "rho_f_sim_m_l_f", "p_f_sim_m_l_f",
                  "rho_f_sim_v_l_f", "p_f_sim_v_l_f",
                  "rho_l_sim_f", "p_l_sim_f",
                  "rho_l_sim_m_f_l", "p_l_sim_m_f_l",
                  "rho_l_sim_v_f_l", "p_l_sim_v_f_l",
                  "z_t_f_sim_m_l_f_and_f_sim_v_l_f",
                  "z_p_f_sim_m_l_f_and_f_sim_v_l_f",
                  "z_t_f_sim_l_and_f_sim_m_l_f",
                  "z_p_f_sim_l_and_f_sim_m_l_f",
                  "z_t_f_sim_l_and_f_sim_v_l_f",
                  "z_p_f_sim_l_and_f_sim_v_l_f",
                  "z_t_l_sim_m_f_l_and_l_sim_v_f_l",
                  "z_p_l_sim_m_f_l_and_l_sim_v_f_l",
                  "z_t_l_sim_f_and_l_sim_m_f_l",
                  "z_p_l_sim_f_and_l_sim_m_f_l",
                  "z_t_l_sim_f_and_l_sim_v_f_l",
                  "z_p_l_sim_f_and_l_sim_v_f_l"]

source_dirs = ["../bibles/Usable/",
               "../bibles/Random_SAME_FLEN/",
               "../bibles/Random_GEOM_LEN/"
              ]

res_names = ["Original",
             "LFP",
             "NLFP"
             ]

def process_bible_properties(languages, thread_id=-1, testing=False, generate_graphs=True):
    unbiased_dataframes = [pd.DataFrame(columns=column_headers),
                             pd.DataFrame(columns=column_headers),
                             pd.DataFrame(columns=column_headers)]
    biased_dataframes = [pd.DataFrame(columns=column_headers),
                           pd.DataFrame(columns=column_headers),
                           pd.DataFrame(columns=column_headers)]
    for i in range(len(source_dirs)): # check the language
        for language in languages:
            new_bible = Bible.from_path(source_dirs[i] + language + ".xml")
            unbiased_dataframe = unbiased_dataframes[i]
            biased_dataframe = biased_dataframes[i]
            #zero_dataframe = zero_dataframes[i]
            if len(new_bible) > 27:
                    new_bible = new_bible.get_new_testament()
            print("({0}) Counted toks: {1}, Reported: {2}, Folder: {3}".format(
                                                new_bible.language,
                                                new_bible.token_count(),
                                                new_bible.reported_word_count,
                                                source_dirs[i]
                                                )
                  )
            # unbiased estimator
            unbiased_result, unbiased_curves = correlation_analysis(
                                                        new_bible,
                                                        language=language.replace(".xml", ""),
                                                        corpus=res_names[i],
                                                        estimator="unbiased", 
                                                        generate_graphs=generate_graphs
                                                )
            print("Unbiased: \n")
            pp.pprint(unbiased_result)
            unbiased_dataframe.loc[language.replace(".xml", "")] = pd.Series(
                                                                unbiased_result
                                                                )
            # biasedstimator
            biased_result, biased_curves = correlation_analysis(
                                                    new_bible,
                                                    language=language.replace(".xml", ""),
                                                    corpus=res_names[i],
                                                    estimator="biased",
                                                    generate_graphs=generate_graphs
                                            )
            print("Biased: \n")
            pp.pprint(biased_result)
            biased_dataframe.loc[language.replace(".xml", "")] = pd.Series(
                                                                biased_result
                                                    )
            if generate_graphs:
                graph(language, res_names[i],
                      "unbiased",
                      unbiased_curves['f_sim_l'],
                      unbiased_curves['f_sim_m_l_f'],
                      unbiased_curves['f_sim_v_l_f'],
                      unbiased_curves['l_sim_f'],
                      unbiased_curves['l_sim_m_f_l'],
                      unbiased_curves['l_sim_v_f_l']
                     )
                graph(language, res_names[i],
                      "biased",
                      biased_curves['f_sim_l'],
                      biased_curves['f_sim_m_l_f'],
                      biased_curves['f_sim_v_l_f'],
                      biased_curves['l_sim_f'],
                      biased_curves['l_sim_m_f_l'],
                      biased_curves['l_sim_v_f_l']
                     )
                
                scales = ['linear', 'log']

                # F plots
                multiple_points = [unbiased_curves['f_sim_v_l_f'],
                                   biased_curves['f_sim_v_l_f']
                                   ]
                multiple_points_legends = [r"Unbiased estimator",
                                           r"Biased estimator", 
                                          ]
                for xscale in scales:
                    for yscale in scales:
                        plot_multiple_points(multiple_points,
                                             multiple_points_legends,
                                             language = language,
                                             sub_folder = r"/F_Sim_V_%sx_%sy/"\
                                                % (xscale, yscale),
                                             corpus_folder = res_names[i],
                                             xlabel = r"$f$",
                                             ylabel = r"$V(l|f)$",
                                             title = r" $V(l|f)$",
                                             xscale = xscale,
                                             yscale = yscale)
    
                multiple_points = [unbiased_curves['f_sim_l'],
                                   unbiased_curves['f_sim_m_l_f']]
                multiple_points_legends = [r"$f\sim l$",
                                           r"$f\sim M(l|f)$"]
                for xscale in scales:
                    for yscale in scales:
                        plot_multiple_points(multiple_points,
                                             multiple_points_legends,
                                             language = language,
                                             sub_folder = r"/F_Sim_M_%sx_%sy/"\
                                                % (xscale, yscale),
                                             corpus_folder = res_names[i],
                                             xlabel = r"$f$",
                                             ylabel = r"$l$",
                                             title = r" $l$",
                                             xscale = xscale,
                                             yscale = yscale)

                # Symmetric case
                multiple_points = [unbiased_curves['l_sim_v_f_l'],
                                   biased_curves['l_sim_v_f_l']
                                   ]
                multiple_points_legends = [r"Unbiased estimator",
                                           r"Biased estimator" 
                                          ]
                for xscale in scales:
                    for yscale in scales:
                        plot_multiple_points(multiple_points,
                                             multiple_points_legends,
                                             language = language,
                                             sub_folder = r"/L_Sim_V_%sx_%sy/"\
                                                % (xscale, yscale),
                                             corpus_folder = res_names[i],
                                             xlabel = r"$l$",
                                             ylabel = r"$V(f|l)$",
                                             title = r" $V(f|l)$",
                                             xscale = xscale,
                                             yscale = yscale)
    
                multiple_points = [unbiased_curves['l_sim_f'],
                                   unbiased_curves['l_sim_m_f_l']]
                multiple_points_legends = [r"$l\sim f$",
                                           r"$l\sim M(f|l)$"]
                for xscale in scales:
                    for yscale in scales:
                        plot_multiple_points(multiple_points,
                                             multiple_points_legends,
                                             language = language,
                                             sub_folder = r"/L_Sim_M_%sx_%sy/"\
                                                % (xscale, yscale),
                                             corpus_folder = res_names[i],
                                             xlabel = r"$l$",
                                             ylabel = r"$f$",
                                             title = r" $f$",
                                             xscale = xscale,
                                             yscale = yscale)
            if testing:
                import ipdb;ipdb.set_trace()
                raise Exception("Testing")
            del new_bible
   
        # Write everything after finishing corpus
        if thread_id > -1:
            unbiased_dataframes[i].to_csv("../results/unbiased_%s.csv_%d" %\
                                          (res_names[i], thread_id))
            biased_dataframes[i].to_csv("../results/biased_%s.csv_%d" %\
                                        (res_names[i], thread_id))
        else:
            unbiased_dataframes[i].to_csv("../results/unbiased_%s.csv" % res_names[i])
            biased_dataframes[i].to_csv("../results/biased_%s.csv" % res_names[i])
            
            stem_folder = "../plots/stem_plots/%s/" % res_names[i]
            stem_plot("../results/unbiased_%s.csv" % res_names[i], stem_folder, "unbiased_L", "L")
            stem_plot("../results/biased_%s.csv" % res_names[i],  stem_folder, "biased_L", "L")
            stem_plot("../results/unbiased_%s.csv" % res_names[i], stem_folder, "unbiased_F", "F")
            stem_plot("../results/biased_%s.csv" % res_names[i],  stem_folder, "biased_F", "F")


def concat_results(max_thread_id):
    for i in range(len(source_dirs)):
        unbiased_dataframe = None
        biased_dataframe = None
        for j in range(0, max_thread_id + 1):
            if j == 0:
                unbiased_dataframe = \
                    pd.read_csv("../results/unbiased_%s.csv_%d" %\
                                (res_names[i], j), index_col=0)
                biased_dataframe = \
                    pd.read_csv("../results/biased_%s.csv_%d" %\
                                (res_names[i], j), index_col=0)
            else:
                unbiased_df =\
                    pd.read_csv("../results/unbiased_%s.csv_%d" %\
                                (res_names[i], j), index_col=0)
                biased_df =\
                    pd.read_csv("../results/biased_%s.csv_%d" %\
                                (res_names[i], j), index_col=0)

                unbiased_dataframe = pd.concat([unbiased_dataframe,
                                                unbiased_df])
                biased_dataframe = pd.concat([biased_dataframe,
                                              biased_df])
            os.remove("../results/unbiased_%s.csv_%d" %\
                                    (res_names[i], j))
            os.remove("../results/biased_%s.csv_%d" %\
                                    (res_names[i], j))

        unbiased_dataframe.to_csv("../results/unbiased_%s.csv" % res_names[i])
        biased_dataframe.to_csv("../results/biased_%s.csv" % res_names[i])
        
        stem_folder = "../plots/stem_plots/%s/" % res_names[i]
        stem_plot("../results/unbiased_%s.csv" % res_names[i], stem_folder, "unbiased_L", "L")
        stem_plot("../results/biased_%s.csv" % res_names[i],  stem_folder, "biased_L", "L")
        stem_plot("../results/unbiased_%s.csv" % res_names[i], stem_folder, "unbiased_F", "F")
        stem_plot("../results/biased_%s.csv" % res_names[i],  stem_folder, "biased_F", "F")

if __name__ == "__main__":
    if len(sys.argv) == 1:
        process_bible_properties(order)
    else:
        thread_group = []
        no_of_threads = int(sys.argv[1])
        assert(no_of_threads > 1)
        total = len(order)
        assert(total >= no_of_threads)
        bibles_per_thread = int(total / no_of_threads)
        thread_id = 0
        for i in range(0, len(order), bibles_per_thread):
            suborder = order[i : i + bibles_per_thread]
            t = multiprocessing.Process(target=process_bible_properties,
                                        args=(suborder, thread_id))
            thread_group.append(t)
            t.start()
            thread_id += 1
        for t in thread_group:
            t.join()
        
        # Uniting
        concat_results(thread_id-1)
