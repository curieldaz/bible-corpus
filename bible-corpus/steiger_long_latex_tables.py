import pandas as pd
import os
import sys
import numpy as np
import string

def create_steiger_latex_table(nullvar_csv_path, zerovar_csv_path, case_label):
    if not os.path.exists(nullvar_csv_path) or \
       not nullvar_csv_path.endswith(".csv"):
        raise TypeError("Not a correct csv file: %s" % nullvar_csv_path )

    if not os.path.exists(zerovar_csv_path) or \
       not zerovar_csv_path.endswith(".csv"):
        raise TypeError("Not a correct csv file: %s" % zerovar_csv_path )

    first_line = True
    nullvar_dataframe = pd.read_csv(nullvar_csv_path)
    zerovar_dataframe = pd.read_csv(zerovar_csv_path)
 
    f_table_contents = ""
    l_table_contents = ""
    for index, _ in nullvar_dataframe.iterrows():
        nullvar_row = nullvar_dataframe.iloc[index]
        zerovar_row = zerovar_dataframe.iloc[index]
        if nullvar_row['Unnamed: 0'] == zerovar_row['Unnamed: 0']:
            row_title = nullvar_row['Unnamed: 0']
            row_title = row_title.replace("%","\%")

            f_table_contents += "\\multirow{2}{*}{\\textsc{%s}}" % row_title
            l_table_contents += "\\multirow{2}{*}{\\textsc{%s}}" % row_title

            # f table contents
            #nullvar
            f_table_contents += " & $V_{nil}$"
            for col in ['z_t_f_sim_m_l_f_and_f_sim_v_l_f', 'z_p_f_sim_m_l_f_and_f_sim_v_l_f',
                        'z_t_f_sim_l_and_f_sim_m_l_f', 'z_p_f_sim_l_and_f_sim_m_l_f',
                        'z_t_f_sim_l_and_f_sim_v_l_f', 'z_p_f_sim_l_and_f_sim_v_l_f']:
                f_table_contents += " & %.2e" % nullvar_row[col]
            f_table_contents += " \\\\\\hhline{~-------}\n"

            #zerovar
            f_table_contents += " & $V_0$"
            for col in ['z_t_f_sim_m_l_f_and_f_sim_v_l_f', 'z_p_f_sim_m_l_f_and_f_sim_v_l_f',
                        'z_t_f_sim_l_and_f_sim_m_l_f', 'z_p_f_sim_l_and_f_sim_m_l_f',
                        'z_t_f_sim_l_and_f_sim_v_l_f', 'z_p_f_sim_l_and_f_sim_v_l_f']:
                f_table_contents += " & %.2e" % zerovar_row[col]
            f_table_contents += " \\\\\\hhline{--------}\n"
 
            # l table contents
            #nullvar
            l_table_contents += " & $V_{nil}$"
            for col in ['z_t_l_sim_m_f_l_and_l_sim_v_f_l',
                        'z_p_l_sim_m_f_l_and_l_sim_v_f_l',
                        'z_t_l_sim_f_and_l_sim_m_f_l',
                        'z_p_l_sim_f_and_l_sim_m_f_l',
                        'z_t_l_sim_f_and_l_sim_v_f_l',
                        'z_p_l_sim_f_and_l_sim_v_f_l']:
                l_table_contents += " & %.2e" % nullvar_row[col]
            l_table_contents += " \\\\\\hhline{~-------}\n"

            #zerovar
            l_table_contents += " & $V_0$"
            for col in ['z_t_l_sim_m_f_l_and_l_sim_v_f_l',
                        'z_p_l_sim_m_f_l_and_l_sim_v_f_l',
                        'z_t_l_sim_f_and_l_sim_m_f_l',
                        'z_p_l_sim_f_and_l_sim_m_f_l',
                        'z_t_l_sim_f_and_l_sim_v_f_l',
                        'z_p_l_sim_f_and_l_sim_v_f_l']:
                l_table_contents += " & %.2e" % zerovar_row[col]
            l_table_contents += " \\\\\\hhline{--------}\n"

    with open("../res/long_f_steiger_table.tex", "r") as f:
        template = f.read()
    template = string.Template(template)
    main_table = template.substitute({'corpus' : case_label,
                                      'table_contents' : f_table_contents})
    result_path = os.path.dirname(nullvar_csv_path) + "/latex_tables/" + \
                  "long_steiger_f_" + case_label + ".tex"
    with open(result_path, "w") as f:
        f.write(main_table)
        f.flush()
    
    with open("../res/long_l_steiger_table.tex", "r") as f:
        template = f.read()
    template = string.Template(template)
    main_table = template.substitute({'corpus' : case_label,
                                      'table_contents' : l_table_contents})
    result_path = os.path.dirname(nullvar_csv_path) + "/latex_tables/" + \
                  "long_steiger_l_" + case_label + ".tex"
    with open(result_path, "w") as f:
        f.write(main_table)
        f.flush()

create_steiger_latex_table("../results/nulvar_LFP.csv",
                            "../results/zerovar_LFP.csv",
                            "LFP")

create_steiger_latex_table("../results/nulvar_NLFP.csv",
                            "../results/zerovar_NLFP.csv",
                            "NLFP")

create_steiger_latex_table("../results/nulvar_Original.csv",
                            "../results/zerovar_Original.csv",
                            "Original")

