from single_csv_analysis import process_dataframe
import os
import sys
import string

if len(sys.argv) < 2 or not os.path.exists(sys.argv[1]):
    raise OSError("Not a valid file")

biased = {}
unbiased = {}
corpora = []
if os.path.isdir(sys.argv[1]):
    for f_name in os.listdir(sys.argv[1]):
        if not f_name.startswith("described_") and\
           f_name.endswith(".csv"):
            path = sys.argv[1] + "/" + f_name
            path = path.replace("//", "/")
            estimator, corpus = f_name.split(".")[0].split("_")
            if corpus not in corpora:
                corpora.append(corpus)
            if estimator == "biased":
                biased[corpus] = process_dataframe(path)
            else:
                unbiased[corpus] = process_dataframe(path)

biased_table_contents = ""
unbiased_table_contents = ""
for corpus in corpora:
    biased_table_contents += "%s " % corpus
    unbiased_table_contents += "%s " % corpus
    for col in ['f_sim_l_lt_f_sim_v_l_f',
                'l_sim_f_lt_l_sim_v_f_l']:
        biased_table_contents += "& %d of 73 or %.2f" % (biased[corpus][col],
                                                         biased[corpus][col] / 73.)
        unbiased_table_contents += "& %d of 73 or %.2f" % (unbiased[corpus][col],
                                                           unbiased[corpus][col] / 73.)
    biased_table_contents += " \\\\\\hline\n"
    unbiased_table_contents += " \\\\\\hline\n"
with open("../res/relationships_table.tex", "r") as f:
    template = f.read()
template = string.Template(template)

unbiased_table = template.substitute({'table_contents' : unbiased_table_contents,
                                      'estimator' : 'unbiased'})
biased_table = template.substitute({'table_contents' : biased_table_contents,
                                    'estimator' : 'biased'})

result_path = "../results/latex_tables/unbiased_relationship_table.tex"
with open(result_path, "w") as f:
    f.write(unbiased_table)
    f.flush()

result_path = "../results/latex_tables/biased_relationship_table.tex"
with open(result_path, "w") as f:
    f.write(biased_table)
    f.flush()
