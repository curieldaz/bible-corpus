import matplotlib.pyplot as plt
import numpy as np
import pandas

import glob

import sys

def concatenate_csv_files(listing, output_name):
    concat_df = None
    first = True
    for filename in listing:
        df = pandas.read_csv(filename,index_col=0)
        if first:
            concat_df = df
            first = False
        else:
            concat_df = pandas.concat([concat_df, df])
    concat_df.to_csv(output_name + ".csv")

if __name__ == '__main__':
    assert(len(sys.argv) > 2)
    
    if len(sys.argv) == 2:
        listing = glob.glob(sys.argv)
    else:
        listing = []
        for i in range(1, len(sys.argv)):
            listing.append(sys.argv[i])

    name = listing[0].split(".")[0]
    concatenate_csv_files(listing, name)
