import numpy as np
import scipy.stats as ss
from bible import Bible

import random

order = ["Latin",
         "Greek",
         "Romanian",
         "Italian",
         "Spanish",
         "Portuguese",
         "French",
         "English",
         "Dutch",
         "Afrikaans",
         "German",
         "Swedish",
         "Norwegian",
         "Danish",
         "Icelandic",
         "Polish",
         "Slovak",
         "Croatian",
         "Serbian",
         "Slovene",
         "Czech",
         "Latvian",
         "Lithuanian",
         "Albanian",
         "Romani",
         "Finnish",
         "Estonian",
         "Hungarian",
         "Paite",
         "Vietnamese",
         "Turkish",
         "Basque",
         "Hebrew",
         "Somali",
         "Tachelhit",#"Tashlhiyt",
         "Kabyle",
         "Wolaytta",
         "Ewe",
         "Zulu",
         "Xhosa",
         "Swahili",
         "Wolof",
         "Lukpa",
         "Dinka",
         "Zarma",
         "Malagasy",
         "Uma",
         "Tagalog",
         "Maori",
         "Cebuano",
         "Indonesian",
         "Galela",
         "Cabecar",#"Cabécar",
         "Chinantec",
         "Amuzgo",
         "Nahuatl",
         "Jakalteko",#"Jakaltek",
         "Q'eqchi'",#"K'ekchí",
         "Uspanteco",#"Uspantek",
         "K'iche'",
         "Cakchiquel",
         "Mam",
         "Quichua",#"Quechua",
         "Aguaruna",
         "Achuar",
         "Shuar",
         "Campa",
         "Barasana",
         "Akawaio",
         "Camsa",#"Camsá",
         "Aukan",#"Ndyuka",
         "Creole",
         "Esperanto"
        ] 

def spearman(points):
    return ss.spearmanr(np.array(points), nan_policy='omit')

if __name__ == '__main__':
    x = np.array(range(10000))
    y = 1/x

    xy_one = []
    xy_zero = []
    for i in range(len(x)):
        xy_one.append((x[i], y[i]))
        xy_zero.append((x[i], np.random.random_sample()))

    rho_one, p_one = spearman(xy_one)
    rho_zero, p_zero = spearman(xy_zero)

    print(rho_one, p_one)
    print(rho_zero, p_zero)
    for language in order:
        bible = Bible.from_path("../bibles/Usable/%s.xml" % language)
        bible_lfp = Bible.from_path("../bibles/Random_SAME_FLEN/%s.xml" % language)
        bible_nlfp = Bible.from_path("../bibles/Random_GEOM_LEN/%s.xml" % language)

        assert(len(bible.all_tokens()) == len(bible_lfp.all_tokens()) == len(bible_nlfp.all_tokens()))
        bible_chars = np.sum([len(tok) for tok in bible.all_tokens()])
        bible_lfp_chars = np.sum([len(tok) for tok in bible_lfp.all_tokens()])
        bible_nlfp_chars = np.sum([len(tok) for tok in bible_nlfp.all_tokens()])
        
        assert(bible_chars == bible_lfp_chars)
        assert(bible_chars != bible_nlfp_chars)
        
        bible_individual_chars = []
        for tok in bible.all_tokens():
            bible_individual_chars += list(tok)
        
        bible_lfp_individual_chars = []
        for tok in bible_lfp.all_tokens():
            bible_lfp_individual_chars += list(tok)
        
        bible_nlfp_individual_chars = []
        for tok in bible_nlfp.all_tokens():
            bible_nlfp_individual_chars += list(tok)
        
        mean_length_bible = 1 + len(bible_individual_chars) / len(bible.all_tokens())
        mean_length_bible_lfp = 1 + len(bible_lfp_individual_chars) / len(bible_lfp.all_tokens())
        mean_length_bible_nlfp = 1 + len(bible_nlfp_individual_chars) / len(bible_nlfp.all_tokens())

        assert(mean_length_bible == mean_length_bible_lfp)
        print(language, mean_length_bible, mean_length_bible_nlfp)

    #import ipdb;ipdb.set_trace()
    #for c in bible_lfp_individual_chars:
    #    bible_individual_chars.remove(c)
    #assert(len(bible_individual_chars) == 0)
