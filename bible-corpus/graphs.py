# -*- coding:utf-8 -*-

import gc

import matplotlib
matplotlib.use("Agg")

from matplotlib import rc
#rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)

import matplotlib.pyplot as plt
import numpy as np
import os

def plot_points(points,
                language,
                sub_folder,
                corpus_folder="all/",
                xlabel=r"$X$",
                ylabel=r"$Y$",
                title=r"Language",
                save=True,
                plot_folder="../plots/individual/",
                logarithmic_x=False,
                logarithmic_y=False,
                scatter=False):

    folder = plot_folder + corpus_folder + sub_folder

    if not os.path.exists(folder):
        os.makedirs(folder)
    if save:
        fig = plt.figure(figsize=(11.69, 8.27))

    if logarithmic_x:
        plt.xscale("log")
    if logarithmic_y:
        plt.yscale("log")

    points = list(set(points))

    # X tokens Y types
    ndataset = np.array(sorted(points))

    # remove nan
    ndataset = ndataset[~np.isnan(ndataset).any(axis=1)]

    if scatter:
        plt.plot(np.array(ndataset[:, 0], dtype=np.int32),
                 np.array(ndataset[:, 1], dtype=np.float),
                 "o",
                 color='blue')
    else:    
        plt.plot(np.array(ndataset[:, 0], dtype=np.int32),
                 #np.array(ndataset[:, 1], dtype=np.int32),
                 np.array(ndataset[:, 1], dtype=np.float),
                 color='blue',
                 lw=2)

    plt.ylabel(ylabel, fontsize=16)
    plt.xlabel(xlabel, fontsize=16)
    plt.title(title, fontsize=16)
    if not logarithmic_x:
        plt.xlim(xmin=0)
    if not logarithmic_y:
        plt.ylim(ymin=0)
    if save:
        plt.savefig(folder + language)
        fig.clf()
    else:
        plt.show()
    plt.close()
    gc.collect()

def plot_multiple_points(curves,
                         curve_legends,
                         language,
                         sub_folder,
                         corpus_folder="all/",
                         xlabel=r"$X$",
                         ylabel=r"$Y$",
                         title=r"Language",
                         xscale="linear",
                         yscale="linear",
                         save=True,
                         plot_folder="../plots/",
                         scatter=False):

    folder = plot_folder + corpus_folder + sub_folder
    title = language + r" -- " + corpus_folder + title

    if not os.path.exists(folder):
        os.makedirs(folder)
    if save:
        fig = plt.figure(figsize=(11.69, 8.27))

    plt.xscale(xscale)
    plt.yscale(yscale)

    colors = ['red', 'blue', 'yellow', 'green']
    line_styles = ["--", ":", "-.", "-"]
    i=0
    for points in curves:

        points = list(set(points))

        # X tokens Y types
        ndataset = np.array(sorted(points))

        # remove nan
        ndataset = ndataset[~np.isnan(ndataset).any(axis=1)]
        if scatter or\
            r"$f\sim l$" == curve_legends[i] or\
            r"$l\sim f$" == curve_legends[i]:
            plt.plot(np.array(ndataset[:, 0], dtype=np.int32),
                     np.array(ndataset[:, 1], dtype=np.float),
                     "o",
                     color=colors[i])
            line_styles[i+1] = "-"
        else:
            plt.plot(np.array(ndataset[:, 0], dtype=np.int32),
                     np.array(ndataset[:, 1], dtype=np.float),
                     color=colors[i],
                     linestyle=line_styles[i],
                     lw=2)
        i += 1

    plt.legend(curve_legends, loc="upper right")
    plt.ylabel(ylabel, fontsize=16)
    plt.xlabel(xlabel, fontsize=16)
    plt.title(title, fontsize=16)
    if xscale == "linear":
        plt.xlim(xmin=0)
    if yscale == "linear":
        plt.ylim(ymin=0)
    if save:
        plt.savefig(folder + language)
        fig.clf()
    else:
        plt.show()
    plt.close()
    gc.collect()
