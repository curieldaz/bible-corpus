# -*- coding:utf-8 -*-

from variables import FMV, LMV
import scipy.stats as ss
import numpy as np
from rpy2.robjects.packages import importr
psych = importr("psych")

from collections import OrderedDict
from graphs import plot_points

def spearman(points):
        return ss.spearmanr(np.array(points), nan_policy='omit')

def graph(language, corpus, estimator, 
          f_sim_l,
          f_sim_m_l_f,
          f_sim_v_l_f,
          l_sim_f,
          l_sim_m_f_l,
          l_sim_v_f_l):
    if estimator == "unbiased":
        title = language + r" -- " + corpus + r" (Unbiased estimator)"
        subfolder = "Unbiased"
    elif estimator == "biased":
        title = language + r" -- " + corpus + r" (Biased estimator)"
        subfolder = "Biased"
    else:
        title = language + r" -- " + corpus + r" (variance to zero)"
        subfolder = "Zero"

    plot_points(f_sim_l,
                language,
                "f_sim_l/%s/" % subfolder,
                corpus_folder=corpus + "/",
                xlabel=r"Frequency $f$",
                ylabel=r"Length $l$",
                title=title,
                logarithmic_x = True,
                logarithmic_y = False,
                scatter = True
               )
    
    plot_points(f_sim_m_l_f,
                language,
                "f_sim_m_l_f/%s/" % subfolder,
                corpus_folder=corpus + "/",
                xlabel=r"Frequency $f$",
                ylabel=r"$M(l|f)$",
                title=title,
                logarithmic_x = True,
                logarithmic_y = False
               )
    
    plot_points(f_sim_v_l_f,
                language,
                "f_sim_v_l_f/%s/" % subfolder,
                corpus_folder=corpus + "/",
                xlabel=r"Frequency $f$",
                ylabel=r"$V(l|f)$",
                title=title,
                logarithmic_x = True,
                logarithmic_y = False
               )
    
    plot_points(l_sim_f,
                language,
                "l_sim_f/%s/" % subfolder,
                corpus_folder=corpus + "/",
                xlabel=r"Length $l$",
                ylabel=r"Frequency $f$",
                title=title,
                logarithmic_x = False,
                logarithmic_y = True,
                scatter = True
               )
    
    plot_points(l_sim_m_f_l,
                language,
                "l_sim_m_f_l/%s/" % subfolder,
                corpus_folder=corpus + "/",
                xlabel=r"Length $l$",
                ylabel=r"$M(f|l)$",
                title=title,
                logarithmic_x = False,
                logarithmic_y = True
               )
    
    plot_points(l_sim_v_f_l,
                language,
                "l_sim_v_f_l/%s/" % subfolder,
                corpus_folder=corpus + "/",
                xlabel=r"Length $l$",
                ylabel=r"$V(f|l)$",
                title=title,
                logarithmic_x = False,
                logarithmic_y = True
               )


def correlation_analysis(D, language="Language", corpus="Original",
                         estimator="unbiased", generate_graphs=False,
                         is_unbiased=False):
    '''Spearman correlation analysis for the
    relationships of interest
    '''
    max_frequency = max(list(D.tokens_by_frequency.keys()))
    max_length = max(list(D.tokens_by_length.keys()))
    f_sim_l, f_sim_m_l_f, f_sim_v_l_f, l_sim_f, l_sim_m_f_l, l_sim_v_f_l = [[],[],[],[],[],[]]
    removed_freqs = []

    # f_sim_l; l_sim_f
    for f in range(1, max_frequency + 1):
        for word_type in D.tokens_by_frequency.get(f, []):
            f_sim_l.append((f, len(word_type)))

    for l in range(1, max_length + 1):
        for word_type in D.tokens_by_length.get(l, []):
            l_sim_f.append((l, D.tok_frequency[word_type]))

    # m_l_f; v_l_f
    for f, _ in f_sim_l:
        if estimator == "unbiased" or estimator == "zero":
            m_l_f, v_l_f = FMV(f, f_sim_l, True)
            if estimator == "zero":
                if np.isnan(v_l_f):
                    v_l_f = 0
        else:
            m_l_f, v_l_f = FMV(f, f_sim_l, False)
        f_sim_m_l_f.append((f, m_l_f))
        f_sim_v_l_f.append((f, v_l_f))

    f_sim_m_l_f = list(set(f_sim_m_l_f))
    f_sim_v_l_f = list(set(f_sim_v_l_f))

    # m_f_l; v_f_l
    for l, _ in l_sim_f:
        if estimator == "unbiased" or estimator == "zero":
            m_f_l, v_f_l = LMV(l, l_sim_f, True)
            if estimator == "zero":
                if np.isnan(v_f_l):
                    v_f_l = 0
        else:
            m_f_l, v_f_l = LMV(l, l_sim_f, False)
        l_sim_m_f_l.append((l, m_f_l))
        l_sim_v_f_l.append((l, v_f_l))

    l_sim_m_f_l = list(set(l_sim_m_f_l))
    l_sim_v_f_l = list(set(l_sim_v_f_l))

    # Spearman
    res={}
    res["rho_f_sim_l"], res["p_f_sim_l"] = spearman(f_sim_l)
    res["rho_f_sim_m_l_f"], res["p_f_sim_m_l_f"] = \
        spearman(f_sim_m_l_f)
    res["rho_f_sim_v_l_f"], res["p_f_sim_v_l_f"] = \
        spearman(f_sim_v_l_f)
    res["rho_l_sim_f"], res["p_l_sim_f"] = spearman(l_sim_f)
    res["rho_l_sim_m_f_l"], res["p_l_sim_m_f_l"] = \
        spearman(l_sim_m_f_l)
    res["rho_l_sim_v_f_l"], res["p_l_sim_v_f_l"] = \
        spearman(l_sim_v_f_l)

    #if res["rho_l_sim_m_f_l"] == res["rho_l_sim_v_f_l"]:
    #    import ipdb;ipdb.set_trace()
    # Steiger's Z
    # f_sim_m_l_f and f_sim_v_l_f

    m_l_f_sim_v_l_f = [list(a) for a in zip([x for _,x in f_sim_m_l_f],
                                            [y for _,y in f_sim_v_l_f])]
    rho_m_l_f_sim_v_l_f, p_m_l_f_sim_v_l_f = spearman(m_l_f_sim_v_l_f)

    steiger = psych.r_test(n=len(f_sim_m_l_f),
                           r12=float(res["rho_f_sim_m_l_f"]),
                           r13=float(res["rho_f_sim_v_l_f"]),
                           r23=float(rho_m_l_f_sim_v_l_f),
                        )
    res["z_t_f_sim_m_l_f_and_f_sim_v_l_f"] = steiger[2][0]
    res["z_p_f_sim_m_l_f_and_f_sim_v_l_f"] = steiger[3][0]

    # f_sim_l and f_sim_m_l_f
    dict_f_sim_l = dict(f_sim_l)
    l_sim_m_l_f = []
    for f, m_l_f in f_sim_m_l_f:
        l_sim_m_l_f.append((dict_f_sim_l.get(f, 0), m_l_f))
    rho_l_sim_m_l_f, p_l_sim_m_l_f = spearman(l_sim_m_l_f)

    #steiger = psych.r_test(n=len(f_sim_m_l_f),
    #                       r12=res["rho_f_sim_l"],
    #                       r13=res["rho_f_sim_m_l_f"],
    #                       r23=rho_l_sim_m_l_f,
    #                    )
    steiger = psych.r_test(n=len(f_sim_l),
                           r12=float(res["rho_f_sim_l"]),
                           r34=float(res["rho_f_sim_m_l_f"]),
                           n2=len(f_sim_m_l_f))
    res["z_t_f_sim_l_and_f_sim_m_l_f"] = steiger[2][0]
    res["z_p_f_sim_l_and_f_sim_m_l_f"] = steiger[3][0]

    # f_sim_l and f_sim_v_l_f
    l_sim_v_l_f = []
    for f, v_l_f in f_sim_v_l_f:
        l_sim_v_l_f.append((dict_f_sim_l.get(f, 0), v_l_f))
    rho_l_sim_v_l_f, p_l_sim_v_l_f = spearman(l_sim_v_l_f)

    #steiger = psych.r_test(n=len(f_sim_v_l_f),
    #                       r12=res["rho_f_sim_l"],
    #                       r13=res["rho_f_sim_v_l_f"],
    #                       r23=rho_l_sim_v_l_f,
    #                    )
    steiger = psych.r_test(n=len(f_sim_l),
                           r12=float(res["rho_f_sim_l"]),
                           r34=float(res["rho_f_sim_v_l_f"]),
                           n2=len(f_sim_v_l_f))
    res["z_t_f_sim_l_and_f_sim_v_l_f"] = steiger[2][0]
    res["z_p_f_sim_l_and_f_sim_v_l_f"] = steiger[3][0]

    # l_sim_m_f_l and l_sim_v_f_l
    m_f_l_sim_v_f_l = [list(a) for a in zip([x for _,x in l_sim_m_f_l],
                                            [y for _,y in l_sim_v_f_l])]
    rho_m_f_l_sim_v_f_l, p_m_f_l_sim_v_f_l = spearman(m_f_l_sim_v_f_l)

    steiger = psych.r_test(n=len(l_sim_m_f_l),
                           r12=float(res["rho_l_sim_m_f_l"]),
                           r13=float(res["rho_l_sim_v_f_l"]),
                           r23=float(rho_m_f_l_sim_v_f_l),
                        )
    res["z_t_l_sim_m_f_l_and_l_sim_v_f_l"] = steiger[2][0]
    res["z_p_l_sim_m_f_l_and_l_sim_v_f_l"] = steiger[3][0]
    
    # l_sim_f and l_sim_m_f_l
    f_sim_m_f_l = [list(a) for a in zip([x for _,x in l_sim_f],
                                        [y for _,y in l_sim_m_f_l])]
    rho_f_sim_m_f_l, p_f_sim_m_f_l = spearman(f_sim_m_f_l)
    
    steiger = psych.r_test(n=len(l_sim_f),
                           r12=float(res["rho_l_sim_f"]),
                           r13=float(res["rho_l_sim_m_f_l"]),
                           r23=float(rho_f_sim_m_f_l)
                          )
    res["z_t_l_sim_f_and_l_sim_m_f_l"] = steiger[2][0]
    res["z_p_l_sim_f_and_l_sim_m_f_l"] = steiger[3][0]

    # l_sim_f and l_sim_v_f_l
    f_sim_v_f_l = [list(a) for a in zip([x for _,x in l_sim_f],
                                        [y for _,y in l_sim_v_f_l])]
    rho_f_sim_v_f_l, p_f_sim_v_f_l = spearman(f_sim_m_f_l)
    steiger = psych.r_test(n=len(l_sim_f),
                           r12=float(res["rho_l_sim_f"]),
                           r13=float(res["rho_l_sim_v_f_l"]),
                           r23=float(rho_f_sim_v_f_l)
                          )
    res["z_t_l_sim_f_and_l_sim_v_f_l"] = steiger[2][0]
    res["z_p_l_sim_f_and_l_sim_v_f_l"] = steiger[3][0]

    #pack points for graphication
    curves = {}
    curves['f_sim_l'] = f_sim_l
    curves['f_sim_m_l_f'] = f_sim_m_l_f
    curves['f_sim_v_l_f'] = f_sim_v_l_f
    curves['l_sim_f'] = l_sim_f
    curves['l_sim_m_f_l'] = l_sim_m_f_l
    curves['l_sim_v_f_l'] = l_sim_v_f_l

    return res, curves

def correlation_dumps(D, estimator):
    '''Builds series for Spearman correlation
    '''
    max_frequency = max(list(D.tokens_by_frequency.keys()))
    max_length = max(list(D.tokens_by_length.keys()))
    f_sim_l, f_sim_m_l_f, f_sim_v_l_f, l_sim_f, l_sim_m_f_l, l_sim_v_f_l = [[],[],[],[],[],[]]
    f_types, f_type_lengths = [OrderedDict({}), OrderedDict({})]
    l_types, l_type_frequencies = [OrderedDict({}), OrderedDict({})]

    # f_sim_l; l_sim_f
    for f in range(1, max_frequency + 1):
        f_types[f] = f_types.get(f, [])
        f_type_lengths[f] = f_type_lengths.get(f, [])
        for word_type in D.tokens_by_frequency.get(f, []):
            f_types[f].append(word_type)
            f_type_lengths[f].append(len(word_type))
            f_sim_l.append((f, len(word_type)))

    for l in range(1, max_length + 1):
        l_types[l] = l_types.get(l, [])
        l_type_frequencies[l] = l_type_frequencies.get(l, [])
        for word_type in D.tokens_by_length.get(l, []):
            l_types[l].append(word_type)
            l_type_frequencies[l].append(D.tok_frequency[word_type])
            l_sim_f.append((l, D.tok_frequency[word_type]))

    # m_l_f; v_l_f
    for f, _ in f_sim_l:
        if estimator == "unbiased" or estimator == "zero":
            m_l_f, v_l_f = FMV(f, f_sim_l, True)
            if estimator == "zero":
                if np.isnan(v_l_f):
                    v_l_f = 0
        else:
            m_l_f, v_l_f = FMV(f, f_sim_l, False)
        f_sim_m_l_f.append((f, m_l_f))
        f_sim_v_l_f.append((f, v_l_f))

    f_sim_m_l_f = list(set(f_sim_m_l_f))
    f_sim_v_l_f = list(set(f_sim_v_l_f))

    # m_f_l; v_f_l
    for l, _ in l_sim_f:
        if estimator == "unbiased" or estimator == "zero":
            m_f_l, v_f_l = LMV(l, l_sim_f, True)
            if estimator == "zero":
                if np.isnan(v_f_l):
                    v_f_l = 0
        else:
            m_f_l, v_f_l = LMV(l, l_sim_f, False)
        l_sim_m_f_l.append((l, m_f_l))
        l_sim_v_f_l.append((l, v_f_l))

    l_sim_m_f_l = list(set(l_sim_m_f_l))
    l_sim_v_f_l = list(set(l_sim_v_f_l))

    return f_types,\
           f_type_lengths,\
           l_types,\
           l_type_frequencies,\
           np.array(f_sim_l),\
           np.array(f_sim_m_l_f),\
           np.array(f_sim_v_l_f),\
           np.array(l_sim_f),\
           np.array(l_sim_m_f_l),\
           np.array(l_sim_v_f_l)
