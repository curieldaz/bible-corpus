import pandas as pd
import os
import sys
import numpy as np
import string

def create_spearman_long_latex_table(nullvar_csv_path, zerovar_csv_path, case_label):
    if not os.path.exists(nullvar_csv_path) or \
       not nullvar_csv_path.endswith(".csv"):
        raise TypeError("Not a correct csv file: %s" % nullvar_csv_path )

    if not os.path.exists(zerovar_csv_path) or \
       not zerovar_csv_path.endswith(".csv"):
        raise TypeError("Not a correct csv file: %s" % zerovar_csv_path )

    first_line = True
    nullvar_dataframe = pd.read_csv(nullvar_csv_path)
    zerovar_dataframe = pd.read_csv(zerovar_csv_path)
 
    fsim_table_contents = ""
    lsim_table_contents = ""
    for index, _ in nullvar_dataframe.iterrows():
        nullvar_row = nullvar_dataframe.iloc[index]
        zerovar_row = zerovar_dataframe.iloc[index]
        if nullvar_row['Unnamed: 0'] == zerovar_row['Unnamed: 0']:
            row_title = nullvar_row['Unnamed: 0']
            row_title = row_title.replace("%","\%")

            fsim_table_contents += "\\multirow{2}{*}{\\textsc{%s}}" % row_title
            lsim_table_contents += "\\multirow{2}{*}{\\textsc{%s}}" % row_title

            #fsim nullvar
            fsim_table_contents += " & $V_{nil}$"
            for col in ['rho_f_sim_l', 'p_f_sim_l',
                        'rho_f_sim_m_l_f', 'p_f_sim_m_l_f',
                        'rho_f_sim_v_l_f', 'p_f_sim_v_l_f']:
                fsim_table_contents += " & %.2e" % nullvar_row[col]
            fsim_table_contents += " \\\\\\hhline{~-------}\n"

            #fsim zerovar
            fsim_table_contents += " & $V_0$"
            for col in ['rho_f_sim_l', 'p_f_sim_l',
                        'rho_f_sim_m_l_f', 'p_f_sim_m_l_f',
                        'rho_f_sim_v_l_f', 'p_f_sim_v_l_f']:
                fsim_table_contents += " & %.2e" % zerovar_row[col]
            fsim_table_contents += " \\\\\\hhline{--------}\n"

            #lsim nullvar
            lsim_table_contents += " & $V_{nil}$"
            for col in ['rho_l_sim_f', 'p_l_sim_f',
                        'rho_l_sim_m_f_l', 'p_l_sim_m_f_l',
                        'rho_l_sim_v_f_l', 'p_l_sim_v_f_l']:
                lsim_table_contents += " & %.2e" % nullvar_row[col]
            lsim_table_contents += " \\\\\\hhline{~-------}\n"

            #lsim zerovar
            lsim_table_contents += " & $V_0$"
            for col in ['rho_l_sim_f', 'p_l_sim_f',
                        'rho_l_sim_m_f_l', 'p_l_sim_m_f_l',
                        'rho_l_sim_v_f_l', 'p_l_sim_v_f_l']:
                lsim_table_contents += " & %.2e" % zerovar_row[col]
            lsim_table_contents += " \\\\\\hhline{--------}\n"
    # f table
    with open("../res/long_f_spearman_table.tex", "r") as f:
        template = f.read()
    template = string.Template(template)
    main_table = template.substitute({'corpus' : case_label,
                                 'fsim_table_contents' : fsim_table_contents
                                     })
    result_path = os.path.dirname(nullvar_csv_path) + "/latex_tables/" + \
                  "long_f_spearman_" + case_label + ".tex"
    with open(result_path, "w") as f:
        f.write(main_table)
        f.flush()

    # l table
    with open("../res/long_l_spearman_table.tex", "r") as f:
        template = f.read()
    template = string.Template(template)
    main_table = template.substitute({'corpus' : case_label,
                                 'lsim_table_contents' : lsim_table_contents})
    result_path = os.path.dirname(nullvar_csv_path) + "/latex_tables/" + \
                  "long_l_spearman_" + case_label + ".tex"
    with open(result_path, "w") as f:
        f.write(main_table)
        f.flush()

create_spearman_long_latex_table("../results/nulvar_LFP.csv",
                                 "../results/zerovar_LFP.csv",
                                 "LFP")

create_spearman_long_latex_table("../results/nulvar_NLFP.csv",
                                 "../results/zerovar_NLFP.csv",
                                 "NLFP")

create_spearman_long_latex_table("../results/nulvar_Original.csv",
                                 "../results/zerovar_Original.csv",
                                 "Original")

